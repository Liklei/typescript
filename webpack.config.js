 const path = require('path');
 const webpack = require('webpack');
 const htmlWepPlugin = require('html-webpack-plugin');
 const {
     CleanWebpackPlugin
 } = require('clean-webpack-plugin'); //清理构建文件夹

 module.exports = {
     mode: 'development',
     entry: {
         class: './src/class.ts',
         ajax: './src/ajax.ts',
         AlgoL: './src/AlgoL.ts',
         Decor: './src/Decorator.ts'

     },
     devtool: 'inline-source-map',
     devServer: {
         port: 6500
     },
     module: {
         rules: [{
                 test: /\.tsx?$/,
                 use: 'ts-loader',
                 exclude: /node_modules/
             },
             {
                 test: /\.js$/,
                 exclude: /node_modules/,
                 loader: "babel-loader"
             },
             {
                 test: /\.css$/,
                 use: ['style-loader', 'css-loader']
             },
             {
                 test: /\.scss$/,
                 exclude: /node_modules/,
                 loaders: ["style", "css", "sass"]
             },
             {
                 test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                 loader: 'url-loader',
                 options: {
                     limit: 10000,
                     name: 'img/[name].[ext]?[hash]',
                     publicPath: '../'
                 }
             }
         ]
     },
     resolve: {
         extensions: ['.tsx', '.ts', '.js']
     },
     plugins: [
         new CleanWebpackPlugin(), // 不用传任何参数
         new webpack.HotModuleReplacementPlugin(),
         new webpack.NamedModulesPlugin(), // HMR shows correct file names in console on update.
         new webpack.NoEmitOnErrorsPlugin(),
         new htmlWepPlugin({
             filename: 'index.html',
             template: './index.html',
             favicon: 'favicon.ico', //添加ico
             inject: true,
             hash: true,
         }),
     ],
     output: {
         filename: '[name].[hash:8].js',
         path: path.resolve(__dirname, 'dist')
     },

 };