require("@babel/register")();
require("@babel/polyfill");
/** 装饰器 */

// 1 类装饰器: 在类声明之前被声明(紧靠着类声明),应用于类构造函数,可以用来监视修改或替换类定义

// 类装饰器: 普通装饰器
function logClass(params: any) {
	params.prototype.apiUrl = 'baidu.com';
	params.prototype.runs = function() {
		console.log('my is run');
	};
}

@logClass
class HttpClient {
	constructor() {}
	getData() {}
}
let hc = new HttpClient();

// 类装饰器: 装饰器工厂(可传参)
function logMy(params:string) {
   return function(target: any){
       console.log(target)
       console.log(params)
   }
}

@logMy('hello')
class HttpClients {
    constructor() { }
    getData() { }
}
let hcc = new HttpClient();


// 重构类构造函数和方法

function logConstr(params:any) {
    return class extends params{ 
        url = "dadsad"
        getData() { 
            console.log(this.url)
        }
    }
}
@logConstr
class LoginClass { 
    public url: string
    constructor(url: string) {
      this.url = url   
    }
    getData() { 
        console.log(this.url)
    }
}

// 属性装饰器
function logProto(params: string) { 
    return function (target:any, attr: string) { 
        target[attr] = params       
    } 
}
@logConstr
class LoginProto { 
    @logProto("xxxx")
    public url: string
    constructor(url: string) {
      this.url = url   
    }
    getData() { 
        console.log(this.url)
    }
}
// 方法装饰器
function logMethods(params: string) { 
    return function (target:any, name: string, desc: any) { 
        let oldMe = desc
        console.log(target[name])
        desc.value = function (...arg:any[]) {
            console.log(params, arg)
            oldMe.value.apply(this, arg)
        }
    }
}
@logConstr
class LoginMethods { 
    @logProto("xxxx")
    public url: string
    constructor(url:string) {
      this.url = url   
    }
    @logMethods('this is my single')
    getData(...arg:any[]) {
        console.log(this.url, arg)
    }
}

let arr = new LoginMethods('xxxfile.com');

console.log(arr.getData(2,3,'dfasdf'));