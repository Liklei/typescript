/** 
 * mongo mysql 底层库
*/
interface DBI<T> {
	add(info: T): boolean;
	update(info: T, id: number): boolean;
	delete(id: number): boolean;
	get(id: number): any[];
}

// 定义操作mysql的类
class MySqlDb<T> implements DBI<T> {
	constructor() {
		console.log('已经和mysql建立连接');
	}
	add(info: T): boolean {
		console.log(info);
		return true;
	}
	update(info: T, id: number): boolean {
		throw new Error('not implements');
	}
	delete(id: number): boolean {
		throw new Error('not implements');
	}
	get(id: number): any[] {
		return [];
	}
}
// 定义操作mssql的类
class MsSqlDb<T> implements DBI<T> {
	constructor() {
		console.log('已经和mssql建立连接');
	}
	add(info: T): boolean {
		console.log(info);
		return true;
	}
	update(info: T, id: number): boolean {
		throw new Error('not implements');
	}
	delete(id: number): boolean {
		throw new Error('not implements');
	}
	get(id: number): any[] {
		return [];
	}
}
//操作用户表，定义一个User类和数据表做映射
class User {
	username: string | undefined;
	password: string | undefined;
	info?: string | undefined;
}

// 实例化对象
let u = new User();
u.username = 'root';
u.password = '31234';

let oSql = new MySqlDb<User>(); // 类作为参数来约束传入的类型

oSql.add(u);
