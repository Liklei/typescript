/**
 * 创建泛型类
 */

/** 
 * ts中实现继承要使用extends、super关键字
*/
interface Cue {
	name: string;
	age: number;
	firstName?: string;
}

class InCue {
	constructor(public config: Cue) {}
}
class Queue<T> {
	private data: T[] = [];
	protected name: string;
	constructor(name: string) {
		this.name = name;
	}
	push = (item: T) => this.data.push(item);
	shift = (): T | undefined => this.data.shift();
	getData = (): T[] => this.data;
}

class Utility {
	protected name: string;
	constructor(name: string) {
		this.name = name;
	}
	reverse<T>(arr: T[]): T[] {
		let toturn: T[] = [];
		for (let i = arr.length - 1; i >= 0; i--) {
			toturn.push(arr[i]);
		}
		return toturn;
	}
}

function reverse<T>(arr: T[]): T[] {
	let toturn: T[] = [];
	for (let i = arr.length - 1; i >= 0; i--) {
		toturn.push(arr[i]);
	}
	return toturn;
}

/** 
 * 抽象类和抽象方法
*/
abstract class Persons {
	//抽象类不能直接被实例化，提供子类的基类
	public name: string;
	constructor(name: string) {
		this.name = name;
	}
	abstract eat(): any; //只能出现在抽象类中
}

class Workers extends Persons {
	constructor(name: string) {
		super(name);
	}
	eat() {//抽象类的子类必须要实现抽象类的方法
		console.log(`the ${this.name} is working`);
	}
}
