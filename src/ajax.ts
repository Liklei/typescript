/** 
 * ajax mock
 * 忽略兼容
 */
interface Config {
	type: string;
	url: string;
	dataType: string;
	data?: string;
}

function Ajax(config: Config): any {
	const xhr: any = new XMLHttpRequest();
	xhr.open(config.type, config.url, true);
	xhr.responseType = config.dataType; // 返回的数据类型
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 304)) {
			//responseText：从服务器获得数据
			console.log(xhr.responseText);
		} else {
			console.log('请求失败！');
		}
	};
	xhr.send(config.data);
}
