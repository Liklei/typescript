const Router = require('koa-router')

// 路由前缀
let routes = new Router({
    prefix: "/api"
});

routes.get('/product', async (ctx, next) => {
    const result = {
        code: 200,
        data: {
            product: 1234,
            desc: 'this module dont support parsing multipart format data, please use co-busboy to parse multipart format data.'
        },
    }
    ctx.response.body = result
});

module.exports = routes;