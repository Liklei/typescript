const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('koa2-cors');
const config = require('./config');

const app = new Koa();
const user = require('./routes/user');
const userDb = require('./db/basic');

//连接数据库
let db = new userDb(config);
db.query();

// 处理跨域相关 https://github.com/zadzbw/koa2-cors

app.use(cors({
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization', 'Date'],
    maxAge: 25000,
    credentials:  true,
    allowMethods: ['GET', 'POST', 'OPTIONS', 'DELETE', 'HEAD', 'PUT'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'X-Custom-Header', 'anonymous']
}))

// 解析request的body
app.use(bodyParser());

// 加载路由
app.use(user.routes()).use(user.allowedMethods());

//启动服务
app.listen(config.HTTP_SERVE_PORT);
console.log(`port listening ${config.HTTP_SERVE_PORT}`)

