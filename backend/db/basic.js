/**
 * 数据库操作
 *
 */
const MySql = require('mysql');

class MQOption {
    constructor(config, name ){
       this.name = name;
       this.config = config
        //连接数据库
       this.connection = MySql.createConnection(this.config.db);
       this.link =false;
       this.init();
    }
    init(){
        this.connection.connect((err) => {
            try{
                this.link = true;
                console.log('connected as id ' + this.connection.threadId);
            }catch (e) {
                console.error('error connecting: ' + err.stack);
            }
        })
    }
    breaklinkling(){
        this.connection.end();
    }
    query(){
        this.connection.query(`SELECT *from user`, (err, rows) => {
            if (err) throw err;
            console.log(rows);
        });
    }
}

module.exports = MQOption;