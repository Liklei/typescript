// 全局代码库声明 提供类型检查和声明，不是具体实现

interface Point{ 
  line: string
  x: number
}

declare let MAX_COUNT: number;
declare let MIN: number;

declare let foo: string | number;

declare function getName(name: string): void

declare class Person {
	static name: string;
  constructor(name: string, age: number | string)
  getNames(id: number): string
}

declare const myPoint: Point
